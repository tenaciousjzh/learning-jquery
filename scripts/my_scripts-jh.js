$(document).ready(function() {
    $("#main.guess_box").remove();
    
    var luckyDiscountBoxNumber = Math.floor((Math.random() * 4) + 1);
    var availableSlots = [1, 2, 3, 4];
    var clickCount = 0;
    
    for (i = 0; i < availableSlots.length; i++) {
        var position = Math.floor((Math.random() * 4) + 1);
        var temp = availableSlots[position];
        availableSlots[position] = availableSlots[i];
        availableSlots[i] = temp;
    }
    
    
    for (i = 0; i < availableSlots.length; i++) {
        var position = availableSlots[i];
        var promotionBox = '';
        
        if (position == luckyDiscountBoxNumber) {
            promotionBox = '<div id="promotional" class="guess_box"><img src="images/jump' + position + '.jpg" /></div>';
        } else {
            promotionBox = '<div id="promotional' + position + '" class="guess_box"><img src="images/jump' + position + '.jpg" /></div>';    
        } 
        
        if (typeof position != "undefined") {
            $("#main").append(promotionBox);   
        }
    }
        
    $(".guess_box").click(function() {
        clickCount++;
        if (clickCount <= 1 && $(this).attr("id") == "promotional") {
            //If there is a previous discount message, we want to remove it so we don't have 2 different discounts showing
            $("#main p").remove();
            //Creates a random number between 1 and 10
            var discountMessage = "<p>Congratulations! Your Discount code is: <strong>JFJ20P</strong></p>";
            $(this).unbind("click");                        
            //Display the message
            $("#main").append(discountMessage);
        } else {
            $("#main p").remove();
            $("#promotional").css("border-color", "#66FF33");
            var unsuccessfulGuessMessage = "<p>Sorry! This was the one you needed to click for the discount. Come back later to try again!</p>";
            $("#main").append(unsuccessfulGuessMessage);
        }
    });    
                
});