$(document).ready(function() {
	$(".guess_box").click(checkForCode);
    
    function checkForCode() {
		
		var discount = getRandom(5);
		var discountMessage = "<p>Your Discount is "+discount+"%</p>";
		$(this).append(discountMessage);
        //After the first click, we don't want to accept any more click events 
        //since the user should only get one chance.
        $(".guess_box").each(function() {
            //unbind each of the clickable boxes (other wise you have to click 
            //once on each of them before they unbind
            $(this).unbind("click");
        });   
	}
    
    function getRandom(basedOnNumber) {
        //Generates a number from 1 to basedOnNumber
        return Math.floor((Math.random() * basedOnNumber));    
    }
    
    $(".guess_box").hover(
        function() {
            //this is the mouse enter event handler
            $(this).addClass("my_hover");
        },
        function() {
            //this is the mouse leave event handler
            $(this).removeClass("my_hover");
        }
    );
});